package ru.kuzin.tm.constant;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kuzin.tm.dto.model.ProjectDTO;
import ru.kuzin.tm.enumerated.Sort;
import ru.kuzin.tm.enumerated.Status;
import ru.kuzin.tm.model.Project;

public interface ProjectConstant {

    int INIT_COUNT_PROJECTS = 5;

    @Nullable
    ProjectDTO NULLABLE_PROJECT = null;

    @Nullable
    Project NULLABLE_PROJECT_MODEL = null;

    @Nullable
    String NULLABLE_USER_ID = null;

    @Nullable
    String EMPTY_USER_ID = "";

    @Nullable
    String NULLABLE_PROJECT_ID = null;

    @NotNull
    String EMPTY_PROJECT_ID = "";

    @NotNull
    Sort CREATED_SORT = Sort.BY_CREATED;

    @NotNull
    Sort NAME_SORT = Sort.BY_NAME;

    @NotNull
    Sort STATUS_SORT = Sort.BY_STATUS;

    @Nullable
    Sort NULLABLE_SORT = null;

    @Nullable
    Integer NULLABLE_INDEX = null;

    @NotNull
    Status IN_PROGRESS_STATUS = Status.IN_PROGRESS;

}