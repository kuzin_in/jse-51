package ru.kuzin.tm.component;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import ru.kuzin.tm.api.IReceiverService;
import ru.kuzin.tm.listener.LoggerListener;
import ru.kuzin.tm.service.ReceiverService;

@NoArgsConstructor
public final class Bootstrap {

    @SneakyThrows
    public void start() {
        @NotNull final ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory(ActiveMQConnectionFactory.DEFAULT_BROKER_URL);
        factory.setTrustAllPackages(true);
        @NotNull final IReceiverService receiverService = new ReceiverService(factory);
        receiverService.receive(new LoggerListener());
    }

}